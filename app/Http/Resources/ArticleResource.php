<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            // 'published' => $this->created_at->format("l,d F Y H:i"),
            'published' => $this->created_at->isoFormat('dddd, D MMMM Y, HH:mm'),
            'subject' => $this->subject->name,
            'author' => $this->user->name,
            'content' => $this->body,
            'update' => $this->updated_at->diffForHumans(),
        ];
    }

    public function with($request)
    {
        return ['status' => 'success'];
    }
}
